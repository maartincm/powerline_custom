# -*- coding: utf-8 -*-
from setuptools import setup, find_packages

install_requires = [
    "libtmux",
]


setup(
    name='powerline_custom',
    version='1.0.0',
    description='Powerline customizations',
    url='https://gitlab.com/Maartincm/powerline_custom',
    author='Martín Nicolás Cuesta',
    author_email='cuesta.martin.n@hotmail.com',
    packages=find_packages(),
    license='AGPL3+',
    install_requires=install_requires,
    zip_safe=False,
)
