from powerline.bindings.tmux import get_tmux_output
import libtmux


def _get_current_tmux_session(pl):
    server = libtmux.Server()
    session_info = get_tmux_output(
        pl, 'list-panes', '-F', '#{session_name}')
    if not session_info:
        return None
    session_name = session_info.rstrip().split('\n')[0]
    session = server.find_where({
        "session_name": session_name,
    })
    return session
