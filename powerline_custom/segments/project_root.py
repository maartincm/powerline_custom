import os
from pathlib import Path

from ..utils import _get_current_tmux_session

ROOT_ENVVAR = "PROJECT_ROOT"


def project_root(pl, dir_shorten_len=False, side="left", *args, **kwargs):
    try:
        session = _get_current_tmux_session(pl)
        env = session.show_environment()
        path = env.get(ROOT_ENVVAR)
        home = Path.home()
        if path.startswith(str(home)):
            fpath = Path("~").joinpath(Path(path).relative_to(home))
            path = str(fpath)
        splitted_path = path.split(os.sep)
        splitted_shortened_path = [
            sub[0:dir_shorten_len] if dir_shorten_len and sub else sub
            for sub in splitted_path[:-1]] + [splitted_path[-1]]
        path = (os.sep).join(splitted_shortened_path)
        contents = path.strip()
    except Exception:
        contents = '?'
    res = [{
        "side": side,
        "contents": "(Root): %s " % contents,
        "highlight_groups": ["date"],
        "divider_highlight_group": None,
    }]
    return res
