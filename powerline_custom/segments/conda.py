from ..utils import _get_current_tmux_session

CONDAENV_ENVVAR = "CONDA_DEFAULT_ENVIRONMENT"


def default_virtualenv(pl, side="left", *args, **kwargs):
    try:
        session = _get_current_tmux_session(pl)
        env = session.show_environment()
        condaenv = env.get(CONDAENV_ENVVAR)
        contents = condaenv.strip()
    except Exception:
        contents = '?'
    res = [{
        "side": side,
        "contents": "(Env): %s " % contents,
        "highlight_groups": ["date"],
        "divider_highlight_group": None,
    }]
    return res
